- ***Universidade Estadual do Oeste do Paraná***
- ***Ciência da Computação - Banco de Dados***
- ***Prof. Alexssander - 30/11/2005***

Banco de Dados: XML, Cliente/Servidor, e Distribuído
====================================================

Autores
-------

- Andre da Silva Barbosa
- Bruno Rafael Hernandes
- Eder Ruiz Maria

Banco de Dados XML
==================

Há dois vertentes quanto banco de dados XML. Um deles é banco de dados não-
relacional convencional que implementa o esquema interno do banco utilizando
xml, ou seja, o amazenamento dos dados realmente ocorre nos arquivos XML e o
SGDB gerencia isso, as consultas podem retornar um arquivo xml ou parte dele que
facilmente é convertido em objeto. Isto é um conceito novo, onde os poucos SGDBs
existentes não são considerados estáveis a ponto de entrar em um ambiente de
produção.

Outro vertente, é o uso de xml para prover o diálogo entre a aplicação e o banco
de dados relacional convencional, onde ocorre o mapeamento entre as tuplas de
uma tabela e as tags de um arquivo xml. Esta abordagem torna interessante quando
analisamos que não é mais necessária a existência de um driver para a aplicação
interagir com o bando de dados, basta a aplicação interagir com o xml. Isto
também possibilita a comunicação entre aplicações de diferentes plataformas,
bastando apenas que ambas compreendam xml.

Banco de Dados Cliente/Servidor
===============================

Banco de dados que pertençam a esse paradigma implementam um servidor onde os
dados ficarão realmente armazenados e um cliente que é uma interface para se
comunicar com o servidor e realizar as consultas. O cliente e o servidor podem
estar fisicamente separados em uma rede. Isso é interessante sob a ótica de que
torna-se possível a criação de aplicações distribuídas onde a persistência dos
dados não fica mais presa a aplicação.

Banco de Dados Distribuído
==========================

Os bancos de dados distribuídos na visão do usuário se assemelha ao
cliente/servidor, onde a distribuição dos dados pelos n nós da rede é
transparente para o usuário. Além da distribuição dos dados pelos nós, pode ser
utilizado a redundância dos dados para implementar a tolerância a falhas e
também usar a redundância de dadosespalhando-os geograficamente para acelerar a
recuperação dos dados.

Banco de dados distribuídos é um conceito ainda em estudo e de difícil
implementação, ainda existe da incompatibilidade entre sistemas que dificulta
ainda mais, existem tentativas de padronizar a comunicação para tornar sua
existência possível, tais quais CORBA.

Alguns banco de dados cliente/servidor utilizam a redundância de dados mais não
distribuem os dados entre os nós.
