CREATE DATABASE gnoia;
USE gnoia;

CREATE TABLE produto (
  cod_produto INT NOT NULL AUTO_INCREMENT,
  descricao VARCHAR(20) NOT NULL,
  PRIMARY KEY(cod_produto)
);

CREATE TABLE marca (
  cod_marca INT NOT NULL AUTO_INCREMENT,
  decricao VARCHAR(20) NOT NULL,
  PRIMARY KEY(cod_marca)
);

CREATE TABLE marca_produto (
  cod_marca INT NOT NULL,
  cod_produto INT NOT NULL,
  PRIMARY KEY(cod_marca, cod_produto),
  FOREIGN KEY(cod_marca)
    REFERENCES marca(cod_marca)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION,
  FOREIGN KEY(cod_produto)
    REFERENCES produto(cod_produto)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION
);

