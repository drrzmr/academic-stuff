Dicionario de Dados
===================

* Entidade: monografia
 - Relacionamento: DB\_monografia, persistencia
 - Elementos de dados: id, titulo, resumo, area, palavra\_chave

* Entidade: aluno
 - Relacionamento: DB\_aluno, persistencia
 - Elementos de dados: id, nome, email

* Entidade: professor
 - Relacionamento: DB\_professor, persistencia
 - Elementos de dados: id, nome, email

* Entidade: emprestimo
 - Relacionamento: DB\_orientacao, persistencia
 - Elementos de dados: monografia.id, aluno.id, professor.id, multa, data

* Entidade: orientacao
 - Relacionamento: DB\_orientacao, persistencia
 - Elementos de dados: professor.id, monografia.id

* Entidade: autoria
 - Relacionamento: DB\_autoria, persistencia
 - Elementos de dados: aluno.id, monografia.id
