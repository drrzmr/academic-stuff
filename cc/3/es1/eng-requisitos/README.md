Engenharia de Software
======================

Autor
-----

- Eder Ruiz Maria


Engenharia de Requisitos - Um Fator de Sucesso em Projetos de Software
----------------------------------------------------------------------

Estudos realizados do Capers Jones revelou que em 75% das empresas a Engenharia
de Requisitos é deficiente. E apesar da evidência de que obter os requisitos é
a parte mais importante e dificil de um projeto de software pouco se sabe sobre
o processo real de especificação de software, a Engenharia de Requisitos apenas
fornece uma descrição básica.

As partes interessadas de todo o sistema computacional podem incluir clientes,
usuários, gerentes de projeto, analistas, colaboradores, a gerência sênior, e a
equipe de funcionários da garantia de qualidade. O projeto de software típico
consiste em um gerente de projeto, analistas, colaboradores, e pessoal da
garantia de qualidade. Um ciclo ER é um conjunto de atividades que contêm ao
menos uma elicitação, modelagem, validação e atividades de verificação.

Foram realizados 7 estudos de campo que relataram na prática Engenharia de
Requisitos. Os autores identificaram a ampla extensão de conhecimento de
território de aplicação como um dos problemas em destaque nos projetos de
software. A pesquisa do Grupo de conhecimento realça o impacto de experiência e
perícia na eficácia da equipe. Os autores identificaram a ampla extensão de
conhecimento de território de aplicação como um dos problemas em destaque nos
projetos de software. Somente algumas equipes de software envolveram
interessados tecnicamente informados tal como empresários de software, garantia
de qualidade, prova, e o pessoal de gerência de configuração cedo no projeto.
Em raros casos que eles foram envolvidos a respeito do projeto, foram
selecionados porque tiveram conhecimento maior de território de aplicação que
seus colegas. Nesses casos, tanto diretores de projeto como membros de equipe
devem aprender o básico como trabalham.

Tradicionalmente, ER recebe uma porcentagem relativamente pequena de recursos
de projeto por todo o ciclo de vida do software. Por exemplo, em 1981 Boehm de
Barry achou que 6 por cento de um custo do projeto e 9 a 12 por cento de
duração de projeto são atribuídos a especificação de requisitos. O projeto
associa-se gastando em média 15,7 por cento de esforço de projeto em atividades
ER. A quantia média de tempo ER igualou 38,6 por cento da duração do projeto.

Investigamos também percepções dos participantes da equipe. As equipes
avaliaram uma coesão relativa de 5.5 nuns 7 na escala de Likert. Em nosso
estudo, as equipes ER focalizado significativamente mais em extrair e modelar
requisitos em vez de validar e verificar os esforços do projeto.

Todas equipes executaram um certo nível de análise de documento. Alguns por
exemplo, como análise de negócio e estudos de mercado enquanto outros derivaram
requisitos de contratos e petições para propostas. A maioria de projetos
desenvolveram protótipos que variam de simples maquetes a protótipos
operacionais. Em outro caso, participantes realçaram a importância de incluir
clientes e operadores em revisões em proporções iguais.

As equipes ER bem sucedido usaram avançandos modelos e métodos tal como modelos
de OO, modelos de conhecimento e matrizes de instalação de função de qualidade
que traduziram “a voz do cliente” em requisitos técnicos quantitativos. No
entanto, eles não abandonaram modelos básicos (por exemplo, o modelo de
entidade-relacionamento nem diagramas de transição de estado). Têm uma
colaboração continuada com interessados assegurar-se de que requisitos são
interpretados adequadamente, lidar com fluxo de requisitos e evitar colapsos de
comunicação. Examinam, por exemplo, artefatos de sistema e material de fonte de
corrente e sistemas prévios. Assim, gerentes de de equipe ER bem sucedido devem
cuidadosamente:

- selecionar membros da equipa habilidosos no território de aplicação, e o
processo de ER;

- sempre designar diretores de projeto capazes de realizar a ER;

- consultar peritos de território e interessados em aumentar e validar a base
de conhecimento da equipe.

As facilidades das atividades ER e equipes bem sucedido freqüentemente são
modelos de especificação de vantagem e exemplos de projetos prévios. Além do
mais, mantêm uma matriz de conjuntos de requisitos de sua origem por através da
especificação e sua implementação. Isto deixa a equipe com seus produtos de
trabalho contribuirem com a satisfação de requisitos.
