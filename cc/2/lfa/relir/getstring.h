/* vim: set ts=4 sw=4: */

#ifndef __GETSTRING_H__
#define __GETSTRING_H__

#define MAX_STR 255
#define ENTER 10

char *getstring(void);

#endif /* __GETSTRING_H__ */
