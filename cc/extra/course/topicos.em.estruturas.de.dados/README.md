Programação
===========

Dia 1 - 09/08
-------------

- Revisão:
  - Conceitos de orientação a objetos
  - Listas simplesmente e duplamente encadeada
  - Fila
  - Pilha
  - Árvores
  - Estratégias de busca em árvores
  - Árvore de Busca Binária
  - Árvore B

Dia 2 - 10/08)
--------------

- Heap
- Árvores AVL
- Árvores Splay

Dia 3 - 11/08
-------------

- Árvores Digitais
- Árvore Trie
- Quadtrees e Octrees

Dia 4 - 12/08
-------------

- Hashes
- Huffman Coding

 
Referências
===========

- Kruse, Robert; Tondo, C. L.; Leung, Bruce. Data Structures & Program Design in
C. 2 ed. Prentice Hall, 1997.

- Eckel, Bruce. Thinking in C++. 2 ed. Mindview, 2000. Disponível em
<http://mindview.net/Books/TICPP/ThinkingInCPP2e.html>

- Dictionary of Algorithms and Data Structures. Disponível em
<http://www.nist.gov/dads/>

- Holte, Robert C. Data Structure Course. Disponível em
<http://www.csi.uottawa.ca/~holte/T26/top.realTop.html>

- Preiss, Bruno R. Data Structures and Algorithms with Object-Oriented Design
Patterns in C++. Disponível em <http://www.brpreiss.com/books/opus4/>
