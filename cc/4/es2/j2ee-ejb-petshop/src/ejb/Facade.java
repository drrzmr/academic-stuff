package ejb;

//import java.rmi.RemoteException;
//import java.util.Collection;

import java.rmi.RemoteException;

import javax.ejb.EJBObject;

public interface Facade extends EJBObject
{

	public void createTipoAnimal(Details info) throws RemoteException;

	public void removeTipoAnimal(Details info) throws RemoteException;
	
/*	public void removeTipoAnimal(Details info) throws RemoteException;
	
	public void addAnimal(Details turma, Details aluno) throws RemoteException;

	public void removeAnimal(Details turma, Details aluno) throws RemoteException;

	public void createServico (Details info) throws RemoteException;
	
	public Collection getAlunosFromTurma(Details turma) throws RemoteException;*/
	
}
