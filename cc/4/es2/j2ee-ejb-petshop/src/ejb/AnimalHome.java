package ejb;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBLocalHome;
import javax.ejb.FinderException;

public interface AnimalHome extends EJBLocalHome
{
	//Faz a criação do EJB.
	public AnimalBean create (int id, String name) throws CreateException;
	//Busca o ejb pela chave primária
	public AnimalBean findByPrimaryKey (int id) throws FinderException;
	//Busca o ejb pela descrição
	public Collection findAll () throws FinderException;
}
