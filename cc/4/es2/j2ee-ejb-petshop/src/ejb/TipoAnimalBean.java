package ejb;

import java.rmi.RemoteException;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.EntityBean;
import javax.ejb.EntityContext;
import javax.ejb.RemoveException;

public abstract class TipoAnimalBean implements EntityBean 
{
	private EntityContext context;
	
	public String ejbCreate(String id, String name) throws RemoteException, CreateException {
		System.out.println("chegou ate o ejbCreate");
		setId(id);
		setName(name);
		return null;
	}

	public void ejbPostCreate(String id, String Name) {
	}
	
	public abstract String getId();
	
	public abstract void setId(String id);
	
	public abstract String getName();
	
	public abstract void setName(String name);

	public void setEntityContext(EntityContext context) throws EJBException, RemoteException
	{
		this.context = context;
	}

	public void unsetEntityContext() throws EJBException, RemoteException
	{
		this.context = null;
	}

	public void ejbRemove() throws RemoveException, EJBException, RemoteException
	{
	}

	public void ejbActivate() throws EJBException, RemoteException
	{
	}

	public void ejbPassivate() throws EJBException, RemoteException
	{
	}

	public void ejbLoad() throws EJBException, RemoteException
	{
	}

	public void ejbStore() throws EJBException, RemoteException
	{
	}
}
