package ejb;

import java.util.Collection;
import java.util.Iterator;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.rmi.PortableRemoteObject;

public class Client {

	public static void main(String[] args) {
		try {
			Collection c;
			Iterator it;
			//Details d;
			
			Context initial = new InitialContext();
			Object ref = initial.lookup("PetShop");
			FacadeHome home = (FacadeHome)PortableRemoteObject.narrow(ref, FacadeHome.class);
			Facade facade = home.create();
			
			Details t1 = new Details("t1", "Cachorrito");
			/*Details t2 = new Details("t2", "turma2");
			Details a1 = new Details("a1", "aluno1");
			Details a2 = new Details("a2", "aluno2");
			Details a3 = new Details("a3", "aluno3");*/
			
			facade.createTipoAnimal(t1);
/*			facade.addAlunoToTurma(t1, a1);
			facade.addAlunoToTurma(t1, a2);
			
			facade.createTurma(t2);
			facade.addAlunoToTurma(t2, a3);*/
			
			//facade.removeTipoAnimal(t1);
/*			facade.addAlunoToTurma(t1, a1);
			
			c = facade.getAlunosFromTurma(t1);
			if (c != null) {
				it = c.iterator();
				while (it.hasNext()) {
					d = (Details)it.next();
					System.out.println("IdAluno: " + d.getId() + ", NomeAluno: " + d.getName());
				}
			} else {
				System.out.println("Nenhum aluno encontrado");
			}*/
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
}
