package ejb;

import javax.ejb.EJBLocalObject;

public interface Animal extends EJBLocalObject
{
	public String getId();
	public String getName();
	
	public TipoAnimal getTipoAnimal();
	
}
