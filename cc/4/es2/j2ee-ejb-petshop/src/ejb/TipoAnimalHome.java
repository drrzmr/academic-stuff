package ejb;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBLocalHome;
import javax.ejb.FinderException;

public interface TipoAnimalHome extends EJBLocalHome
{
	//Faz a criação do EJB.
	public TipoAnimal create (String id, String name) throws CreateException;
	//Busca o ejb pela chave primária
	public TipoAnimal findByPrimaryKey (String id) throws FinderException;
	//Busca o ejb pela descrição
	public Collection findAll () throws FinderException;
}
