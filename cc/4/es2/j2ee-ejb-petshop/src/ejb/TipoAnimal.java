package ejb;

import javax.ejb.EJBLocalObject;

public interface TipoAnimal extends EJBLocalObject
{
	public String getId();
	public String getName();

}
