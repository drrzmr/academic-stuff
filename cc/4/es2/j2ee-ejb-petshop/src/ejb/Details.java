package ejb;

import java.io.Serializable;

public class Details implements Serializable {
	private String id;
	private String name;
	
	public Details(String id, String name) {
		this.id = id;
		this.name = name;
	}
	
	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
