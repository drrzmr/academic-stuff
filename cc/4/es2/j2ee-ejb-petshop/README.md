- ***Trabalho de Engenharia de Software II***
- ***Professor: Willian Francisco da Silva***
- ***Data: 06/06/05***

Objetivo
--------

O objetivo do trabalho é desenvolver uma pequena aplicação utilizando EJB’s
(Enterprise Java Beans).

Requisitos
----------

* O domínio do sistema fica a critério da equipe.

* Devem ser escolhidos casos de uso com os seguintes tipos de relacionamento no
banco de dados: 1-1, 1-n e n-n.

* A aplicação não deve ter interface gráfica (pode ser no próprio Eclipse ou
DOS).

* Devem ser realizadas consultas do tipo:

 - Recupera todo o conteúdo de uma tabela

 - Recupera um item específico do banco

 - Recupera um item e os seus sub-itens (nr. venda -> itens de venda)

* A aplicação deve obrigatoriamente implementar persistência CMP (Container
managed persistence).

* Devem ser implementados os seguintes padrões J2EE Value Object (ou Transfer
Object) e o Session Facade.

* O servidor de aplicação a ser utilizado pode ser o Jboss, Resin ou outro
servidor de aplicação.

Instruções
----------

* O trabalho deve ser feito em equipe de dois alunos.

* A data de entrega é dia 06 de Julho.

* O trabalho deve ser entregue em CD devidamente rotulado. No CD, os artefatos a
serem entregues são: Diagrama de classes, Diagrama de seqüência (para todos os
cenários) e Código-Fonte.

* Os diagramas devem ser feitos na ferramenta Rational Rose.
