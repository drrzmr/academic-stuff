- ***Universidade Estadual do Oeste do Paraná***
- ***Ciência da Computação - Redes de Computadores***
- ***2005***

Camada de Enlace de Dados do FDDI 
=================================

Autores 
-------

- Eder Ruiz Maria
- Natália Ghattas

Introdução
==========

O padrão FDDI (*Fiber Distributed Data Interface*) foi estabelecido pelo ANSI
(*American National Standards Institute*) em 1987. Este abrange o nível físico e
de ligação de dados. As redes FDDI adoptam uma tecnologia de transmissão
idêntica com as redes Token-Ring (IEEE 802.5) utilizando fibra ótica, ou seja
utiliza o token passing como protocolo de acesso ao meio, mas ao contrário do
*Token Ring*, permite a existência de mais de um datagrama no meio físico. Sua
utilização usual é como um backbone de alta velocidade devido ao seu suporte a
grandes larguras de banda e maiores distancias que o cobre. FDDI usa uma
arquitetura dual-ring com tráfego em cada anel em direções opostas. O anel
duplo é formado pelo anel primário e pelo de anel de secundário correndo em
direções opostas. O anel de secundário é ativado no caso de falha no anel
primário. A ativação do anel secundário é controlada pelo protocolo FDDI, que é
capaz de detectar o segmento do anel primário que apresenta a falha, isolando o
mesmo através da interligação dos anéis primário e secundário nas extremidades
do segmento com falha.

![Ring 1](ring1.gif)

![Ring 2](ring2.gif)

![Ring 3](ring3.gif)

Especificações FDDI A FDDI especifica a porção física e de acesso a dados do
modelo de referência OSI. FDDI não é uma única especificação, mas sim uma
coleção de quatro especificações separadas, cada uma com uma função específica.
Combinadas, essas especificações tem a capacidade de prover conectividade de
alta velocidade entre protocolos como o TCP/IP e o IPX, e meio de transmissão
como o cabo de fibra ótica. As quatro especificações são o Controle de Acesso
ao Meio (MAC), Protocolo de Camada Física (PHY), Physical-Medium Dependent
(PMD), e Gerenciamento de Estação (SMT). A especificação MAC define como o meio
é acessado, incluindo o formato do quadro, manipulação de token, endereçamento,
algoritmos para calculos de verificação CRC e mecanismos de recuperação de
erros.

A especificação PHY defien os procedimentos de codificação/decodificação de
dados, exigências de clock e enquadramento entre outros. A especificação PMD
define as caracteristicas do meio de transmissão, como linkd de fibra ótica,
níveis de força, velocidade de bit de erros, compontentes óticos e contectores.
A especificação SMT define a configuração de estações FDDI, configurações do
anel, e características de controle de anel, incluindo inserção e remoção de
estações, inicialização, isolamento de falhas e recuperação, escalonamento e
coleção de estatísticas. FDDI é similar ao IEEE 802.3 Ethernet e ao IEEE 802.5
Token Ring, com suas relações ao modelo OSI. O principal propósito é prover
conectividade entre protocolos das camadas de cima do OSI e seu meio usado para
conectar dispositivos de rede.

Meio de transmissão
===================

FDDI usa a fibra ótica como meio de transmissão primário, porém pode usar
também cabos de cobre (CDDI). A fibra ótica tem várias vantagens sobre o fio de
cobre, é mais seguro, confiável e a performance é melhor pois a fibra não emite
sinais elétricos. O meio físico que emite sinais elétricos, podem ser
“emendados”, assim permitindo o acesso não autorizado aos dados transmitidos.
Além disso, a fibra é imune a interferência eletromagnética (EMI). A fibra
ótica suporta maiores larguras de banda que o cobre, apesar de já ter fios de
cobre que transmite até 100Mbps. Finalmente, FDDI permite que as estações
tenham até 2km de distância entre elas usando a fibra multimodo, e ainda
distâncias maiores usando a monomodo. FDDI define dois tipos de fibra ótica:
monomodo e multimodo. O modo é o raio de luz que entra na fibra em um ângulo.
Multimodo usa um LED como gerador de luz, enquanto o monomodo usa um laser. A
fibra multimodo permite que vários modos de luz se propaguem pela fibra. Por
causa desse modo da luz entrar em vários ângulos, chegarão em tempos diferentes
na fibra. Essa característica é conhecida como dispersão modal, isso limita a
largura de banda e distâncias que podem ser usadas com fibra multimodo. Por
essa razão, a fibra multimodo é geralmente usada para conectividade em um
prédio ou ambiente geograficamente pequeno.

Transmissão de bits
===================

FDDI usa pulsos leves para transmitir informação de uma estação para a próxima.
A menor unidade de informação que o FDDI manipula é o bit. Antes de executar
qualquer atividade de alto nível, é necessário ue seja possível enviar um bit
através de uma fibra (ou fio) e ter que ou outro lado reconheça o bit que foi
enviado. Um bit pode ter dois valores, ou é zero ou um. No FDDI é expressado
pela mudança de estado da luz no outro lado. Aproximadamente a cada 8
nanossegundos, a estação recebe uma luz vindo de outra máquina. A luz pode
estar ligada ou desligada. Se mudou desde o ultimo estado, isso é traduzido no
bit um. Se a luz não mudou desde o último estado, é considerado o bit zero.

![Bitstream](bitstream.gif)

Codificação dos Símbolos O FDDI faz transferencia de dados atravéz de . Que são
seqüências de 5 bits, que quando juntado com outro simbolo forma um byte. Estes
5 bits fornecem 16 símbolos de dados (0-F) e mais 8 símbolos de controle (Q, H,
I, J, T, R, S) e também 8 símbolos de violação (V).

Codificação dos Símbolos
========================

O FDDI faz transferencia de dados atravéz de simbolos. Que são seqüências de 5
bits, que quando juntado com outro simbolo forma um byte.

Estes 5 bits fornecem 16 símbolos de dados (0-F) e mais 8 símbolos de controle
(Q, H, I, J, T, R, S) e também 8 símbolos de violação (V). 

Símbolo          | Sequência de bits
-----------------|------------------
0 (binario 0000) | 11110
1 (binario 0001) | 01001
2 (binario 0010) | 10100
3 (binario 0011) | 10101
4 (binario 0100) | 01010
5 (binario 0101) | 01011
6 (binario 0110) | 01110
7 (binario 0111) | 01111
8 (binario 1000) | 10010
9 (binario 1001) | 10011
A (binario 1010) | 10110
B (binario 1011) | 10111
C (binario 1100) | 11010
D (binario 1101) | 11011
E (binario 1110) | 11100
F (binario 1111) | 11101
Q                | 00000
H                | 00100 (alguns símbolos V também podem ser iguais ao H)
I                | 11111
J                | 11000
K                | 10001
T                | 01101
R                | 00111
S                | 11001
V or H           | 00001
V or H           | 00010
V                | 00011
V                | 00101
V                | 00110
V or H           | 01000
V                | 01100
V or H           | 10000

Formato do Token
================

![Token](token.gif)

- **Preâmbulo (PA)**: Dá uma sequência única que prepara cada estação para um
quadro que está por vir.

- **Delimitador de início (SD)**: Indica o início de um quadro por empregar um
padrão de sinalização que diferencia do resto do quadro.

- **Controlador de frame (FC)**:Indica o tamanho dos campos de endereço e se o
quadro contém dados assíncronos ou síncronos, entre outras informações de
controle.

- **Delimitador de Fim (ED)**: Contém símbolos únicos, não podem ser símbolos de
dados que indicam o fim do quadro.

Uma estação ganha o direito transmitir informação ao meio quando detecta um
token passing no meio. O token é um sinal de controle formado por uma seqüência
única de símbolos que circulam pelo meio após cada transmissão de informação.
Qualquer estação, que detecte um token, pode remor o token do anel e começar a
transmitir um ou mais frames ao meio, quando concluir a transmissão a estação
então emite um novo token cedendo o direito de transmissão a outra estação.

Formato do Frame
================

![Frame](frame.gif)

- **Preâmbulo (PA)**: Dá uma sequência única que prepara cada estação para um
quadro que está por vir.

- **Delimitador de início (SD)**: Indica o início de um quadro por empregar
um padrão de sinalização que diferencia do resto do quadro.

- **Controlador de frame (FC)**: Indica o tamanho dos campos de endereço e se
o quadro contém dados assíncronos ou síncronos, entre outras informações de
controle.

- **Endereço de Destino (DA)**: Contém endereçamento unicast, multicast ou
broadcast. Como nos endereços no Ethernet e no Token Ring, os endereços do
FDDI são de 6 bytes.

- **Endereço Fonte (SA)**: Identifica a estação única que enviou o quadro.
Que também é um endereçamento de 6 bytes.

- **Dados (INFO)**: Contém informações enviadas a protocolos de camadas
superiores ou informação de controle.

- **Sequência de Controle de Quadro (FCS)**: É arquivado pela estação fonte com
um valor de redundância cíclica calculado dependente do conteúdo do quadro
(assim como Ethernet e Token Ring). O endereço de destino recalcula o valor
para determinar se o quadro foi danificado, se for, o quadro é descartado.

- **Delimitador de Fim (ED)**: Contém símbolos únicos, não podem ser símbolos de
dados que indicam o fim do quadro.

- **Status do Quadro (FS)**: Permite que a estação fonte determine se um erro
ocorreu, identifica se o quadro foi reconhecido e copiado por uma estação
destino.
