#!/bin/bash
#<ini> <variancia> <max> <n_amostras>

INI=$1
VARIANCIA=$2
MAX=$3
N_FDS=$4
N_AMOSTRAS=$5

UBENCH="./ubench"

declare REF

error()
{
	echo $1
	exit 1
}

get_teste(){ #

	N=0
	RET=0
	while [ $N -le $N_AMOSTRAS ]; do

		AUX=$($UBENCH $1 $2)
		RET=`echo $RET+$AUX | bc`
		let N=$N+1
	done
}

# <n> <n_group> <time>
bench(){ #$log_file

	I=$INI
	while [ $INI -le $MAX ]; do

	
		echo -n "$INI " | tee -a $1
		echo -n "$I " | tee -a $1
		let I=$I+1
		get_teste $INI $N_FDS
		let INI=$INI+$VARIANCIA
		echo $RET | tee -a $1
	done
} 

if [ $# = 6 ]
then
	echo "#<$1> <$2> <$3> <$4> <$5> <$6>" | tee $6
	bench $6
else
	error "uso: $0 <ini> <var> <max> <n_fds> <n_amostras> <log_file>"
fi
