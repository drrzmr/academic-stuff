/*
 * reflex.c - flexible benchmark for Linux SMP scheduler
 *
 * Copyright (C) 2000 IBM
 *
 * Written by Shailabh Nagar (nagar@us.ibm.com) 29 Jan 2001
 * Based on sched_test_yield.c written by Bill Hartner (bhartner@us.ibm.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * 29 Jan 2000 - 1.0.0 Initial version. Shailabh Nagar
 *
 *  9 Feb 2000 - 1.1  
 *          Active sets implemented
 *
 *
 */

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sched.h>
#include <errno.h>
#include <wait.h>
#include <math.h>
#include <getopt.h>
#include <time.h>


#define STACK_SIZE	(8192)
#define CLONE_FLAGS	(CLONE_VM | CLONE_SIGHAND | CLONE_FS | CLONE_FILES)
#define DEF_PERCENT	(1)
#define NUM_WARMUP	(1)
#define MIN_TRIALS	(NUM_WARMUP+5)
#define MAX_TRIALS	(25)

#define MAX_CHILDREN    (8192)


/* Macros related to active set formation 
 * depend on setsize,num_active,setid 
 * setid is a per-thread value that should be defined properly
 * before macros depending on it are called 
 */

#define isextra(myid)   (myid >= (regsetsize * num_active))      
#define idextra(setid)  ((regsetsize * num_active) + setid)
#define needextra(setid) (setid < num_children - (regsetsize*num_active))

#define regsetstart(setid)  (regsetsize * setid)
#define regsetend(setid)    ((regsetsize * (setid+1)) - 1)


/*
 * system calls
 */

int __clone (int (*fn) (void *arg), void *thread_stack, int flags, void *arg);

/*
 * prototypes for this file.
 */

void run_test_time(void); 
int bouncer(void *arg);
int (*worker) (void *arg);
double local_exec(void) ;
double probrange(unsigned long top);
void calibration(void) ;
float variance(int n, float sum, float sum2);
double child_avg();
double child_var();
int confidence(int iterations);
double uniform(double mean);
void usage(void);


char *child_stack ;

struct timezone tz1;
struct timeval  tv1;
struct timezone tz2;
struct timeval  tv2;
struct timeval  tvr;



int num_children = 200;	        /* number of child processes to create */
int num_active ; 
int num_seconds = 10 ;          /* number of seconds to run test */
int foutput = 0;                /* controls type of output */
int fquiet = 0;                 /* controls type of output */
int rnd_compute_time = 10;      /* number of microseconds of computation per
				 * round if thread doesn't yield later 
				 */

int start_sem;			/* sem to serialize test */
int stop_test = 0;		/* flag to end test when convergence met */
int valid_test = 1;
int TOKENSZ;                    /* size of message treated as token */
double rounds_per_microsecond = 0.0 ; /* obtained through calibration */
int local_exec_count = 0;       /* unused */

int comp_nonyield_rounds ;      /* number of rounds of execloop for nonyield*/ 
int comp_yield_rounds    ;      /* number of rounds of execloop for yield*/ 

int weight_reschedule_idle = 75 ; /* determines weightage to be given to the 
				   * reschedule_idle() path of the scheduler
				   * by making calls to blocking reads
				   */

double probyield;               /* probability of calling sched_yield 
				 * == (100-weight_reschedule_idle)/100  
				 */

double probyieldmult;           /* == probyield/[1-probyield)*(1-probyield)] */




int childpipe[MAX_CHILDREN][2]; /* pipes used to bounce token around */

float tau[MAX_TRIALS-1] = { 12.706, 4.303, 3.182, 2.776, 2.571, 2.447, 2.365, 2.306, 2.262, 2.228, 2.201, 2.179, 2.160, 2.145, 2.131, 2.120, 2.110, 2.101, 2.093, 2.086, 2.080, 2.074, 2.069, 2.064 };


struct _results
{
	float data;
	float ave;
	float var;
	float conf;
} results[MAX_TRIALS] = {{0.0}};

struct _total
{
	unsigned long long count;
	char pad[24];
} *total;


int main(int argc, char *argv[])
{
	int c;
	int clone_err;
	int i,rc;
	int exit_rc = 0;
	int wait_status;
	int iterations = 0;
	struct sched_param param;
	struct rlimit myrlimit;
	struct sembuf mysembuf1;

	num_active = num_children ; /* In case its unspecified */

	while ((c = getopt(argc, argv, "c:t:a:w:r:o:q")) != -1) {
	  switch (c) {
	  case 'c': num_children = atoi(optarg); break;
	  case 't': num_seconds = atoi(optarg); break;
	  case 'a': num_active = atoi(optarg); break;
	  case 'w': weight_reschedule_idle = atoi(optarg); break;
	  case 'r': rnd_compute_time = atoi(optarg); break; 	  
	  case 'o': foutput = atoi(optarg); break;
	  case 'q': fquiet = 1; break;
	  default:  usage();
	    goto exit_main2;
	  }
	}


	
	
	if ((num_seconds <= 0) ||
	    (num_children <= 0) || (num_children > MAX_CHILDREN) ||
	    (num_active <= 0) || (num_active > num_children) ||
	    (weight_reschedule_idle < 0) || (weight_reschedule_idle > 100) ||
	    (rnd_compute_time < 0)
	   )
	    
	{
	    	usage();
		goto exit_main2;
	}
	

	/* Increase limits on number of open files */
	/* normally 1024 (cur & max), set to MAX_CHILDREN */

	myrlimit.rlim_cur = myrlimit.rlim_max = MAX_CHILDREN*2 ; 

	if (setrlimit(RLIMIT_NOFILE,&myrlimit) != 0) {
		exit_rc = errno ;
		perror("setrlimit() ");
		goto exit_main2;
	}
	
	/* allocate childrens stacks*/
	
	child_stack = malloc(num_children*STACK_SIZE);
	if (child_stack == NULL) {
		exit_rc = errno;
		perror ("malloc of 'child_stack' failed ");
		goto exit_main2;
	}
	
	/* open num_children pipes */
	for (i=0; i< num_children; i++) {
		if (pipe(childpipe[i]) < 0) {
			exit_rc = errno;
			perror ("pipe() ");
			goto exit_main3;
		}
	}

        /* calibrate internal loops */
	calibration();
	probyield = local_exec_count ;
	TOKENSZ = sizeof(char); 
	probyield = (100.0-(double)weight_reschedule_idle)/100.0 ; 
	probyieldmult = probyield / ((1-probyield)*(1-probyield)); 

	
	/*
	comp_nonyield_rounds = (int) (uniform(rnd_compute_time) * rounds_per_microsecond) ;
	comp_yield_rounds = (int) (comp_nonyield_rounds * probyieldmult) ; 
	printf("Sample subrounds : nonyield %d yield %d, probyield %0.3f\n",
	   comp_nonyield_rounds, comp_yield_rounds, probyield); 
	*/

	/* start_sem is used to start all children at same time */	

	start_sem = semget (IPC_PRIVATE, 1, IPC_CREAT | IPC_EXCL | 0660);
	if (start_sem == -1) {
		exit_rc = errno;
		perror("semget(start_sem) IPC_CREATE ");
		goto exit_main4;
	}
	
	/* allocate/initialize statistic variables */
	
	total = malloc(num_children*sizeof(struct _total));
	if (total == NULL) {
		exit_rc = errno;
		perror ("malloc of 'total' failed ");
		goto exit_main3;
	}
	for (i = 0 ; i < num_children ; i++)
		total[i].count = 0;


	/* Launch threads */
  	worker = bouncer;
	for (i=0; i< num_children; i++) {
		clone_err = __clone(worker, 
				    &child_stack[(i+1)*STACK_SIZE],
				    CLONE_FLAGS,
				    (void*)i);
		if (clone_err == -1) {
			exit_rc = errno;
			perror ("clone() ");
			goto exit_main5;
		}
		// printf("\t\tLaunched child %d\n",i);

	}

	/* Increase priority of parent thread */

	param.sched_priority = 90;
	rc = sched_setscheduler(getpid(), SCHED_FIFO, &param);
	if (rc == -1) {
		exit_rc = errno;
		perror ("sched_setscheduler() ");
		goto exit_main5;
	}

	run_test_time();
	

exit_main5: 
	/* wait until all children complete */	
	for (i = 0 ; i < num_children ; i++) {
		rc = waitpid (-1, &wait_status, __WCLONE);
		if (rc == -1) {
			exit_rc = errno;
			perror ("waitpid() ");
		}
	}

exit_main4:
 
	rc = semctl(start_sem, 0, IPC_RMID, 0);

exit_main3:

	/* explicitly close all pipes : unnecessary */
	for (i=0; i< num_children; i++) {
		close(childpipe[i][0]);
		close(childpipe[i][1]);
	}
	free(child_stack);
	
exit_main2:
	return (exit_rc) ;
}


int bouncer(void *arg)
{
	int i=0, rc, exit_rc = 0, do_yield;
	int myid,nextid,previd;
	struct sembuf mysembuf;

	char pbuf[100]="Hello\n" ;
	int msgsize ;

	double p1 ;

	int regsetsize,setid ;

	
	/* Active set formation : 
	 * each id put into sets of size "regsetsize" first
	 * leftover ids distributed amongst regular sets, one per set
	 * e.g.  to divide [0..10] into num_active=3,
	 *       form regular sets : [0,1,2] [3,4,5] [6,7,8] 9,10
	 *       9,10 are "extras", distribute one each to regular sets 
	 *	          forming    [0,1,2,9] [3,4,5,10] [6,7,8]
	 *	 and adjust previd,nextid appropriately	to form logically 
	 *       circular linked lists per set such as (for first set)
	 *                 0 <--> 1 <--> 2 <--> 9 <--> 0 
	 */


	/* Defaults */
	myid = (int) arg ;
	nextid = (myid+1)%num_children;
	previd = (myid+num_children-1)%num_children ;

	/* Determine extras */
	regsetsize = num_children/num_active ;

	if (isextra(myid))
		setid = myid - (regsetsize * num_active) ;
	else
		setid = myid / regsetsize ;
	
	/* Fit extras into appropriate sets */
	if (needextra(setid)) {
		if (myid == regsetstart(setid))
			previd = idextra(setid);
		if (myid == regsetend(setid))
			nextid = idextra(setid);
		if (isextra(myid)) {
			previd = regsetend(setid);
			nextid = regsetstart(setid);
		}
	}
	else {
		if (myid == regsetstart(setid))
			previd = regsetend(setid);
		if (myid == regsetend(setid))
			nextid = regsetstart(setid);
	}
	


	/* Adjust nextid to decide "active" sets */
	/* HACK to make groups of two threads each 
	   if (myid%2)
	   nextid = previd ;
	*/	

	/*
	printf("[%d] : setid %d setstart %d setend %d prev %d next %d\n",
	       myid,setid,regsetstart(setid),regsetend(setid),previd,nextid);
	
	return(0);
	*/

 start:
	/* wait to be released */
	mysembuf.sem_num = 0;
	mysembuf.sem_op  = -1;
	mysembuf.sem_flg = 0;
	
	rc = semop(start_sem, &mysembuf, 1);
	if (rc == -1) {
		exit_rc = errno;
		perror ("child semop(start_sem) failed");
		return(exit_rc);
	} 

	/* Actions to be done by each thread */

	/* printf("\t\t\tChild %d starting nextid %d \n",myid,nextid); */


#if 0	
	while (!stop_test) {
		
                /* Blocking read */
		msgsize = read(childpipe[myid][0],pbuf,TOKENSZ) ;
		
		/* printf("\t[%d] read : %s\n",myid,pbuf); */

		comp_nonyield_rounds = (int) (uniform(rnd_compute_time) * rounds_per_microsecond) ;

		if (probrange(100) < weight_reschedule_idle) {
			for (i=0;i<comp_nonyield_rounds;i++) 
				local_exec();
		} else { /* Yield */

			
			comp_yield_rounds = (int) (comp_nonyield_rounds * probyieldmult) ;
			do {
				for (i=0;i<comp_yield_rounds;i++) 
					local_exec();
				sched_yield();
			} while ((probrange(100) >= weight_reschedule_idle) && !stop_test) ;
			
			
		}
				
 		/* "awaken" next thread in group */
		write(childpipe[nextid][1],pbuf,TOKENSZ);
		
		/* Record statistic */
		total[myid].count++;
	}
#endif
	while (!stop_test) {
		
                /* Blocking read */
		msgsize = read(childpipe[myid][0],pbuf,TOKENSZ) ;
		
		/* printf("\t[%d] read : %s\n",myid,pbuf); */

		do {
			comp_nonyield_rounds = (int) (uniform(rnd_compute_time) * rounds_per_microsecond) ;
			for (i=0;i<comp_nonyield_rounds;i++) 
				local_exec();
		
			do_yield = probrange(100) > weight_reschedule_idle ;
			if (do_yield) {
				sched_yield();
				total[myid].count++;
			}

		} while (do_yield && !stop_test);

 		/* "awaken" next thread in group */
		write(childpipe[nextid][1],pbuf,TOKENSZ);
		/* Record statistic */
		total[myid].count++;
	}




	/* to wake blocked neighbours */
	write(childpipe[nextid][1],pbuf,TOKENSZ);


	return(exit_rc);
}



void process_data(void)
{
	int i;
	double grand ;

	printf("#Statistics\n[id]\t%15s\n","Count");
	for (i=0 ; i < num_children; i++) {
		printf("[%d]\t%15d\n",i,total[i].count);
		grand += total[i].count ;
	}
	
	grand /= num_children*1.0 ;
	printf("#Avg\t%15.2lf\n",grand);
}


void usage(void)
{

	printf ("Usage: reflex -c num_child [-t num_sec] [-a num_active] [-w weight_reschedule] [-r rnd_compute_time] [-o N] [-q]\n\n");
	printf ("version 1.0.0  nagar@us.ibm.com \n");
	printf ("-c N, number of child processes to create (should be EVEN)\n");
	printf ("-t N, number of seconds to run test before checking throughput.\n");
	printf ("-a N, number of concurrently active child processes.\n");
	printf ("-w N, 100 <= N <= 0 weight for favoring reschedule_idle() loop. \n"); 
	printf ("-r N, number of microseconds of computation to perform before passing token, assuming no yield is done.\n");
	printf ("-o N, how to report results.\n");
	printf ("      0 default reporting of results.\n");
	printf ("      1 tablular output.\n");
	printf ("      2 for -t, prints 'children , seconds , microseconds/round'\n");
	printf ("-q  , removes some printfs\n");
}


void run_test_time(void)
{
	int i,rc;
	int exit_rc = 0;
	int iterations = 0;
	unsigned long long x;
	unsigned long long y, prev_y, total_yields = 0;
	struct sembuf mysembuf1;
	char pbuf[100]="1";
	int regsetsize; 
	
	mysembuf1.sem_num = 0;
	mysembuf1.sem_op  = num_children;
	mysembuf1.sem_flg = 0;
	
	/* post the sema4 to allow children to start */

	rc = semop (start_sem, &mysembuf1, 1);
	if (rc == -1) {
		stop_test = 1;
		exit_rc = errno;
		perror ("parent semop(start_sem) failed ");
		goto exit_test;
	}

	regsetsize = num_children/num_active ;

	/* start off by writing to one child in each active set */
	for (i=0; !isextra(i) && i<num_children ; i+=regsetsize) {
		/* printf("PARENT :      waking %d initially\n",i); */
		write(childpipe[i][1],pbuf,TOKENSZ);
	}

	/* Shortcircuit 
	   sleep(num_seconds);
	   stop_test = 1 ; 
	   return ;
	*/
	
restart:

	if (!fquiet) printf (".");

	prev_y = 0;
	y = 0;
	
	/* get the start time */

	rc = gettimeofday (&tv1, &tz1);
	if (rc) {
		stop_test = 1;
		exit_rc = errno;
		perror ("gettimeofday failed on tv1 ");
		goto exit_test;
	}

	/* wait until timeout */
	
	for (i = 0 ; i < num_children ; i++) prev_y += total[i].count;
 	sleep (num_seconds);
	for (i = 0 ; i < num_children ; i++) y += total[i].count;
	// printf("Rerun : Across children : Avg %15.2f \t Var %15.2f\n",child_avg(),child_var());

	/* get end time */

	rc = gettimeofday (&tv2, &tz2);
	if (rc) {
		stop_test = 1;
		exit_rc = rc;
		perror ("gettimeofday failed on tv2 ");
		goto exit_test;
	}
	total_yields += y;
	total_yields -= prev_y;
	
	/* compute microseconds per yield */

	timersub(&tv2, &tv1, &tvr); /* tvr now contains result of tv2-tv1 */

	x = (unsigned long long)tvr.tv_sec * 1000000;
	x+= (unsigned long long)tvr.tv_usec;
	results[iterations].data = (float)x;
	results[iterations].data /= (float)(y - prev_y);

	iterations++;
	if (confidence(iterations)) {
		stop_test = 1;
	} else {
		goto restart;
	}

	if (!fquiet) printf (" Test Completed.\n");

	switch (foutput) {
		
	case 1:
		printf ("   microsec         ave    variance  confidence\n");
		printf ("----------- ----------- ----------- -----------\n");
		for (i = 0 ; i < iterations ; i++) {
			printf ("%11.3f %11.3f %11.3f %11.3f ",
				results[i].data,
				results[i].ave,
				results[i].var,
				results[i].conf);
			if (i < NUM_WARMUP)
				printf ("warmup");
			printf ("\n");
		}
		break;
	case 2:
		if (!valid_test) break;
		printf ("%u , %u , %0.3f\n", num_children, num_seconds, results[iterations-1].ave);
		break;
		
	default:
		if (!valid_test) break;
		printf ("%u children did %Lu rounds at %0.3f microseconds/round\n", num_children, total_yields, results[iterations-1].ave);
		break;
	}
	
exit_test:	
	return;	
}


double local_exec()
{
	unsigned int a = 0, b=0;
	//	memcpy(&a,&b,1);
	local_exec_count++;
}



void calibration(void)
{
	/* figure out how many loops we can execute per micro second */
	int i;
	int count = 0;
	unsigned long n_initial = 100000;
	unsigned long clock1, clock2, clockterm;

	clock1 = clock() ;
	clockterm = clock1 + 5*CLOCKS_PER_SEC;
	do {
		for(i=0; i< n_initial; i++)
		{
			local_exec();
		}
		clock2 = clock();
		count++;
	} while (clock2 < clockterm);

	n_initial *= count;

	rounds_per_microsecond = ((double)n_initial*CLOCKS_PER_SEC) / ((double)(clock2-clock1) * 100000.0);

//	rounds_per_microsecond = (n_initial*1000000) / ((double)(clock2-clock1)*CLOCKS_PER_SEC);
	/* printf(">> [%ld] %ld %ld %lf\n",CLOCKS_PER_SEC,n_initial,(clock2-clock1),rounds_per_microsecond); */
}

/******************** Statistical functions **********************************/

double probrange(unsigned long top)
{
	double value = random();
	value = (top*value) / ((double)RAND_MAX);
//	printf("%d %lf\n",top,value);
	return value;
}

float variance(int n, float sum, float sum2)
{
	return ((((float)n * sum2)-(sum * sum))/((float)n * (float)(n - 1)));
}

/*
 * iterertions includes NUM_WARMUP + a min of 3 iterations
 */
int confidence(int iter)
{
	float	sum_trials = 0.0;
	float	sum_trials2 = 0.0;
	float	percent = (float)DEF_PERCENT / 100.0;
	int	i,x,y;
	
	x = iter - NUM_WARMUP;
	y = iter - 1;

	/* compute average */

	if (iter <= NUM_WARMUP) {
		return(0);
	} else {
 		for (i = NUM_WARMUP ; i < iter ; i++) {
			sum_trials  += results[i].data;
			sum_trials2 += results[i].data * results[i].data;
		}
	}
	results[y].ave = sum_trials / (float)(x);

	if (iter < NUM_WARMUP + 2)
		return(0);

	/* compute the variance */

	results[y].var = variance(x,sum_trials,sum_trials2);
	if (isnan(results[y].var))
		results[y].var = 0.0;
	if (results[y].var < 0.0)
		results[y].var = 0.0;

	/* 95% confident that ave is within percent% of "true" average ? */

	results[y].conf = tau[x-2] * sqrt(results[y].var / (float)x);
	if (isnan(results[y].conf))
		results[y].conf = 0.0;
	if (results[y].var < 0.0)
		results[y].var = 0.0;

	if (iter < MIN_TRIALS)
		return(0);

	if (results[y].conf <= results[y].ave * percent) return (1);

	return(1);

	if (iter == MAX_TRIALS) {
		valid_test = 0;
		printf("\n*****> failed to reach confidence1 level <*****\n");
		return(1);
        } else {
		return(0);
	}
}


double child_var(void) 
{
	int i;
	double mean=0.0,meandiff=0.0,sumdiff=0.0 ;
	
	mean = child_avg();
	for (i = 0 ; i < num_children ; i++) {
		meandiff = (double)total[i].count - mean ; 
		sumdiff += meandiff*meandiff;
	}

	return (sumdiff/((double)num_children - 1));
}

double child_avg(void) 
{
	int i;
        double sum_trials=0.0;

	for (i = 0 ; i < num_children ; i++) 
		sum_trials  += (double) total[i].count;

	return (sum_trials/(double)num_children);
}

double uniform(double mean)
{
	/* Generate a random uniformly in [0.5xmean,1.5xmean] */
	double value = random();
	value = ((value/(double)RAND_MAX) + 0.5) * mean ; 
	return value ;
}
	
