/* vim: set ts=4 sw=4: */

#include <modbus.h>
#include "parport.h"

int
teste_lampada()
{
	struct porta *pp;
	pp = parport_create();
	parport_open(pp);
	dispositivo_teste_on(pp);
	sleep(1);
	dispositivo_teste_off(pp);
	parport_close(pp);
}

void
answer_sended(int function, int address, int length)
{
	struct porta *pp;
	pp = parport_create();
	parport_open(pp);

	/*
	 *printf("packet sended : function 0x%x \t address 0x%x \t length %d\n",
	 *        function,address,length);
	 */
	printf("funcao: %i\n", function);

	switch (function) {

	case 0x41:
		dispositivo_teste_on(pp);
		break;
	case 0x42:
		dispositivo_teste_off(pp);
		break;
	case 0x43:
	default:
		printf("funcao nao reconhecida :(\n");

	}
	parport_close(pp);
}

int
main()
{
	teste_lampada();
	int device;

	/* allocate the modbus database */
	Mbs_data = (int *) malloc(255 * sizeof(int));
	Mbs_data[0] = 0x00;

	/* open device */
	device = Mb_open_device("/dev/ttyS3", 9600, 0, 8, 1);

	/* print debugging informations */
	Mb_verbose = 1;

	/* start slave thread with slave number=1 */
	Mb_slave(device, 1, NULL, NULL, answer_sended);

	/* hit <return> to stop program */
	getchar();
	Mb_slave_stop();
	Mb_close_device(device);

	return 0;
}
