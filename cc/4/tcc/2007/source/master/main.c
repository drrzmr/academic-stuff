/* vim: set ts=4 sw=4: */

#include <stdlib.h>
#include <stdio.h>
#include <modbus.h>
#include "linha_de_comando.h"

int
send(Mbm_trame trame, int *data_in, int *data_out)
{
	int result;
	Mb_verbose = 1;

	result = Mb_master(trame, data_in, data_out, NULL, NULL);
	/* return 0 if ok */
	if (result < 0) {
		if (result == -1)
			printf("error : unknow function\n");
		if (result == -2)
			printf("crc error\n");
		if (result == -3)
			printf("timeout error\n");
		if (result == -4)
			printf("error : bad slave answer\n");
	} else {
		/* printf("ok\n"); */
		if (trame.function == 0x43) {
			printf("status: ");
			if (data_out[0] == 0)
				printf("desligado\n");
			else
				printf("ligado\n");
		}
	}
	return 0;
}

int
master(int funcao, int slave)
{

	int device;
	Mbm_trame trame;
	int data_in[256];
	int data_out[256];
	char data[256];

	/* open device */
	device = Mb_open_device("/dev/ttyS0", 9600, 0, 8, 1);
	/* setup master packet */
	trame.device = device;
	trame.function = funcao;	/* send data */
	trame.slave = slave;		/* slave number */
	trame.timeout = 10000;

	send(trame, data_in, data_out);
	Mb_close_device(device);
	return 0;
}

int
main(int argc, char *argv[])
{
	struct gengetopt_args_info arg;
	if (cmdline_parser(argc, argv, &arg) != 0)
		exit(EXIT_FAILURE);

/*
	printf("uhet\n");
	printf("%i\n", arg.escravo_arg);
	printf("%i\n", arg.dispositivo_arg);
	printf("%i\n", arg.funcao_arg);
*/

	if ((arg.funcao_arg >= 65) && (arg.funcao_arg <= 67)) {

		master(arg.funcao_arg, arg.escravo_arg);

	} else {
		printf("funcao nao implementada :(\n");
		exit(0);
	}

	cmdline_parser_free(&arg);
	return 0;
}
