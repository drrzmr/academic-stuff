\chapter{Fundamentação Teórica}
\label{cap:ft}

\section{Sistemas Embarcados}

A evolução dos computadores tem proporcionado máquinas cada vez mais poderosas,
com maior capacidade de processamento e armazenamento. Acompanhando esta
evolução, os programas de computador também aumentam em tamanho e complexidade
\cite{se1}.

Em contraste à abundância de recursos computacionais dos computadores de uso
geral, existe uma demanda crescente por dispositivos portáteis de baixo custo,
obtidos através da redução de recursos \cite{se1}.

Soluções computacionais, que utilizam um computador para um fim específico,
necessitam de equipamentos mais compactos, dotados dos recursos estritamente
necessários para o funcionamento do sistema. O software deve ser compatível com
os recursos do equipamento, além de atender aos requisitos do sistema
\cite{se2}.

Um sistema embarcado, ou um sistema embutido, é uma combinação de
\textit{hardware}, \textit{software} e periféricos dedicados a realizar uma
determinada tarefa. Um sistema embarcado pode ser acoplado a um sistema maior
para realizar uma função específica, como controlar um robô, ou pode ser um
sistema independente. Sistemas embarcados se diferenciam de sistemas de
propósito geral por terem uma finalidade específica. São exemplo de sistemas
embarcados: dispositivos móveis portáteis, terminais-caixa, terminais de
auto-atendimento bancário, terminal de acesso a internet, quiosque multimídia,
\textit{thin-clients}, decodificadores de televisão a cabo, sistemas de controle
industrial, entre outros \cite{se2}.

Dentro da categoria de sistemas embarcados encontra-se uma infinidades de
categorias com requisitos múltiplos. Um terminal de acesso a internet necessita
de vídeo de alta resolução, interface gráfica com o usuário, e dispositivo de
rede. Um sistema dedicado ao controle do sistema de freios de um veículo deve
suportar \textit{hardware} específicos como sensores e atuadores, além de exigir
maior confiabilidade \cite{se1}.

Os sistemas embarcados podem ser classificados de acordo com outras
características criando variantes como sistemas embarcados com requisitos de
tempo real, ou sistemas embarcados com limitações de recursos de processamento e
memória \cite{se1}.

\subsection{Sistemas Operacionais Embarcados}

Os sistemas embarcados não são padronizados, e muitos dos componentes de
hardware usados em um sistema embarcado não são comuns em outros sistemas. A
grande variedade de configurações e a presença de componentes de hardware que
não são padronizados, faz com que a maioria dos sistemas embarcados possuam um
Sistema Operacional desenvolvido especificamente para a plataforma \cite{se2}.

Existe uma grande quantidade de sistemas comerciais, que são desenvolvidos com o
foco na generalidade para atender a um domínio de aplicação. Devido a grande
diversidade de configurações encontradas em sistemas embutidos, um Sistema
Operacional Comercial geralmente é concebido para atender a um determinado nicho
dentro de uma determinada família de sistemas embarcados. Alguns Sistemas
Operacionais são destinados a sistemas de tempo real, e outros a uma família de
dispositivos com características comuns \cite{se2}.

Geralmente um Sistema Operacional de propósito geral tem custo inferior, e tende
a ser mais confiável do que um outro que seja desenvolvido especificamente para
um sistema. Porém pode ser difícil obter um Sistema Operacional adequado, que
atenda aos requisitos do sistema, e suporte os recursos de hardware da
plataforma \cite{se2}.

\subsection{Linux em Sistemas Embarcados}

No mercado de Sistemas Embarcados o Linux tem relevante importância,
principalmente pelo custo de entrada baixo. Como o Linux é Software Livre e
gratuito, não demanda pagamento de \textit{royalties}, o usuário não prescisa
arcar com o custo do Sistema Operacional quando estiver comprando um
eletrodoméstico equipado com um Sistema Embarcado \cite{taurion}.

Devido a esta situação, o Linux está se tornando o sistema dominante no ambiente
de sistemas embarcados. Recentes pesquisas apontam que os desenvolvedores de
sistemas embarcados estão migrando ou planejando a migração para Linux,
substituindo os sistemas proprietários que anteriormente dominavam este mercado.
Empresas como Red Hat e MontaVista estão se especializando para ocupar este
nicho do mercado \cite{taurion}.

A liderança do Linux no mercado de sistemas embarcados tem grande importância, à
medida que entre no mercado milhões de dispositivos (em número muito maior que
os computadores convencionais), acarretará uma forte influência nas plataformas
tradicionais. Podendo levar muitos desenvolvedores de aplicações para o ambiente
Linux convencional adaptar seus conhecimentos para desenvolver também aplicações
para qualquer outra plataforma \cite{taurion}.

O sucesso do Linux neste cenário, entre outros fatores, deve-se ao fato do
mercado de software embarcado possuir peculiaridades específicas, como uma ampla
diversidade de funcionalidades e uma gama muito grande de processadores. Ao
passo que em ambientes de computação tradicional existe uma concentração em
poucos processadores e arquiteturas \cite{taurion}.

O software embarcado deve apresentar alta estabilidade. Uma aeronave ou uma
usina nuclear não pode apresentar falhas no software. Outra característica de
muitos dispositivos é a necessidade de operação em tempo real, principalmente
nos equipamentos de controle de processo \cite{taurion}.

O software embarcado deve operar em ambientes de recursos computacionais
limitados, como memória ou discos magnéticos. Assim, recursos como gerenciamento
de discos, rotinas de grande impacto no desempenho de sistemas comerciais,
torna-se de pouca importância no contexto da computação embarcada
\cite{taurion}.

O fator custo é de grande importância em sistemas embarcados. Um telefone
celular passa a não ser economicamente viável se custar o dobro de outro por
causa do preço do software básico que esteja embutido nele. O sistema
operacional de um equipamento destes é totalmente invisível e não desperta a
percepção do comprador. As diferenças devem ser claramente perceptíveis, como um
maior número de funcionalidades disponibilizadas. Um sistema operacional baseado
nos princípios do Software Livre tira um peso grande do custo, tornando-se
extremamente atraente para este setor \cite{taurion}.

Devido a qualidade que o Linux possuí \cite{taurion}, tornou-se peça importante
da arquitetura de tecnologia de sistemas embarcados militares dos Estados Unidos
da América. A Marinha americana baseou sua arquitetura, denominada NOACE
(\textit{Navy Open Architecture Computing Environment}) em padrões abertos. A
Marinha americana também considera o Linux fundamental em sua estratégia de
reduzir os tempos e custos de desenvolvimento de seus sistemas embarcados em
navios \cite{taurion}, aeronaves e submarinos. O primeiro grande projeto da
Marinha norte-americana a adotar esta tecnologia é a nova classe de
\textit{destroyers} DD(X), cujo primeiro navio é o DDG 1000, Zunwalt. O
\textit{software} destes navios vai rodar em servidores \textit{Blade} da IBM,
que também contribuiu decisivamente para adaptar os ambientes Linux e Java para
operar em tempo real. O mesmo conceito de padrões abertos foi adotado pelo
Exército americano em sua arquitetura chamada de WSCOE (\textit{Weapons Systems
Common Operating Environment} \cite{taurion}.

De maneira geral, os usuários (consumidores) destes equipamentos não sabem e nem
precisam saber que o Linux é o Sistema Operacional rodando em seus dispositivos.
Mas para os fabricantes o fato do Linux estar disponível em código fonte,
permite que seja usado sem pagamento de \textit{royalties} para cada produto
vendido, além de facilitar em muito a customização, praticamente montando-o para
as necessidades especificas de cada aparelho. Isto dá aos fabricantes um maior
controle sobre o dispositivo, permitindo fazer modificações e implementar
inovações muito rapidamente. \cite{taurion}

As afirmações apontadas acima quanto a utilização de Linux em Sistemas
Embarcados, são confirmadas pela pesquisa anual intitulada \textit{Snapshot of
the embedded Linux market} realizada pelo site
\textit{http://www.linuxdevices.com}. O uso de Sistemas Operacionais para
Sistema Embarcado é ilustrado na Figura \ref{fig:ld1}. A prospecção do uso no
passado e no futuro de Linux como Sistema Operacional nos Sistemas Embarcados é
ilustrada na Figura \ref{fig:ld2}.

%\clearpage
\figura{Sistemas Operacionais utilizados \cite{linuxdevices}}{ld1}{images/ld1}
{0.75}

\clearpage

\figura{Passado e futuro do uso de Linux \cite{linuxdevices}}{ld2}{images/ld2}
{0.75}

\section{Sistemas Industriais}

Sistemas industriais caracterizam-se por ter muitos produtores de dados (robôs
medindo temperatura, e esteira controlando velocidade) e também muitos
consumidores (controladores, sistemas supervisórios, e estações de engenharia).
Essas estruturas representam, tipicamente, sistemas distribuídos.
Desenvolvedores de aplicações distribuídas dispõem de muitas alternativas para
escolher, dentre elas, a que melhor atende às suas necessidades específicas.
Dentre as arquiteturas de comunicação encontradas no mercado, destacam-se:
Ponto-a-ponto, Mestre-Escravo, Cliente-Servidor e Produtor-Consumidor \cite{si}.

\subsection{Ponto-a-Ponto}

A comunicação ponto-a-ponto é a forma mais simples de comunicação. Nessa
arquitetura, o dispositivo que inicia a comunicação deve conhecer o endereço do
dispositivo de destino. Uma vez que a conexão tenha sido estabelecida, os dois
dispositivos podem ter um diálogo de comunicação em duas vias
\cite{livro:tanem}.

No entanto, uma conexão ponto-a-ponto não é adequada se um dispositivo deve
realizar o intercâmbio de dados com mais de um destinatário, simultaneamente.
Esta arquitetura suporta a comunicação do tipo ``um-para-um''
\cite{livro:tanem}.

\subsection{Mestre-Escravo}

A solução mestre-escravo é uma das mais utilizadas em ambientes industriais, no
nível de processo. É uma arquitetura centralizada na qual todas as estações
dependem de um mestre. Neste caso a estação que opera como mestre decide quando
cada uma das outras, denominadas escravas, tem o direito de transmitir.
Geralmente as estações escravas comunicam-se apenas com o mestre
\cite{livro:tanem}.

A arquitetura mestre-escravo é determinística, porém apresenta diversos
inconvenientes quanto ao atendimento de tempos de resposta, consistência
espacial e temporal de dados, e facilidades de manutenção \cite{livro:tanem}.

\subsection{Cliente-Servidor}

Redes cliente-servidor têm uma estação servidora de dados que pode conectar-se
simultaneamente a muitas estações clientes. Portanto, a arquitetura
cliente-servidor pode ser vista como uma arquitetura ``muitos-para-um''
\cite{livro:tanem}.

A arquitetura cliente-servidor funciona bem quando todas as estações da rede
necessitam acessar informações centralizadas. Entretanto, se a informação está
sendo gerada e consumida por múltiplos dispositivos, esta arquitetura requer que
toda a informação seja enviada ao servidor antes de tornar-se acessível aos
clientes. Isto além de ineficiente também adiciona-se um \textit{delay}
desconhecido (e portanto indeterminístico) ao sistema, porque o cliente receptor
não conhece quando novas informações são adicionadas ao servidor
\cite{livro:tanem}.

\subsection{Produtor/Consumidor}

Com uma arquitetura produtor-consumidor as mensagens podem ser transmitidas
diretamente entre os dispositivos conectados através da rede. Um dispositivo
pode ser ou um consumidor dos dados que ele precisa, ou um produtor de
informações que são utilizadas por ele próprio ou por outras estações da rede
\cite{livro:tanem}.

Os dados são enviados sem um endereço de destino exato. Eles são recebidos por
todos os dispositivos e cada um deles decide independentemente (respeitando o
identificador do tópico) se está interessado nessa mensagem ou não. Esta
arquitetura não é adequada para tráfego do tipo pedido/resposta, como acontece
com transferência de arquivos \cite{livro:tanem}.

Esses sistemas são bons na distribuição de grandes quantidades de informações
temporária de tempo crítico, mesmo na presença de mecanismos de entrega não
confiáveis. e podem manipular muitos padrões de fluxo de dados
\cite{livro:tanem}.

\section{Arquitetura de Controle Industrial}

O suporte de comunicação de um ambiente industrial típico é formado por diversos
níveis hierárquicos, constituindo uma estrutura que envolve desde as tarefas
administrativas até o controle da operação das máquinas e equipamentos de
produção. A integração e o intercâmbio de informações entre os níveis
produtivos, portanto, dependem de recursos de comunicação adequados, que devem
levar em conta as diferentes restrições temporais e requisitos associados ao
funcionamento das atividades características de cada um desses níveis
\cite{gaucho}.

Segundo \apud{pimentel}{gaucho}, esses níveis podem ser modelados como uma
pirâmide, que define a hierarquia de controle da organização. A Figura
\ref{fig:arqind} representa um sistema simplificado, com três níveis
hierárquicos.

\figura{Hierarquia de Controle Industrial \cite{gaucho}}{arqind}{images/arqind}
{0.75}

\subsection{Nível de Campo}

Na hierarquia do sistema de comunicação industrial, o nível mais baixo, neste
trabalho denominado nível de campo, tem a responsabilidade de disponibilizar
informações sobre o estado dos equipamentos de produção e atuar sobre os mesmos
de acordo com instruções de controle vindas do nível imediatamente superior
(nível de controle), ou de outros dispositivos inteligentes do mesmo nível, no
caso de controle distribuído \cite{gaucho}.

Neste nível os dados trafegam por redes conhecidas como redes de campo
(fieldbuses). As informações características deste nível envolvem o transporte
de variáveis de processo, envio de set-points de máquinas, aquisição do estado
dos equipamentos, atuação sobre motores, válvulas e outros equipamentos
\cite{gaucho}.

As redes de campo, portanto, suportam a interação dos diversos dispositivos de
monitoração e controle existentes em uma planta de produção industrial. Através
delas estes dispositivos realizam o intercâmbio de informações necessárias à
coordenação do funcionamento da planta. Este nível requer um alto grau de
determinismo no tráfego de informações que, em geral, é realizado através de
leitura e escrita de dados nos dispositivos, em taxas de transmissão compatíveis
com os requisitos do processo controlado \cite{gaucho}.

Uma das características mais importantes de uma rede de campo é o seu protocolo
de comunicação. De uma forma geral, essas redes empregam protocolos de
comunicação digital, seriais e bi-direcionais, para a interligação física e
lógica dos sensores e atuadores entre si e com os controladores \cite{gaucho}.

Além das restrições temporais, a especificação das redes de campo deve
considerar as restrições ambientais características do chão-de-fábrica, adotando
tecnologias resistentes a condições agressivas, tais como temperaturas elevadas,
interferências eletromagnéticas, poeira, gases, etc. Geralmente adotam, como
meio físico, pares de fio de cobre trançados (meio elétrico), fibras ópticas ou
rádio (wireless), dependendo da solução adotada para o processo de produção e
das características do ambiente \cite{gaucho}.

As vantagens técnicas e econômicas apresentadas pelas redes de campo em sistemas
industriais tiveram como conseqüência o aparecimento de diversas soluções
proprietárias, a maioria delas incompatíveis entre si. Observa-se uma demanda do
mercado por soluções padronizadas, de modo a garantir a interoperabilidade entre
sistemas e componentes produzidos por diferentes fabricantes e desenvolvedores,
o que resultou num esforço de padronização, a nível internacional
\apud{tovar}{gaucho}.

\subsection{Nível de Controle}

O nível de controle é responsável pela execução das tarefas de supervisão e
controle do processo, com base nas informações intercambiadas com o nível de
campo. Ou seja, ele responde pela coordenação da operação dos equipamentos e
dispositivos que compõem a linha de produção \cite{gaucho}.

No caso de sistemas de controle centralizado, este nível executa os cálculos de
controle e otimização, bem como a aquisição de dados, monitoração dos processos,
registros de alarmes e manutenção de equipamentos. Já para sistemas com controle
distribuído, que representam a atual tendência dos sistemas de controle
industrial, os cálculos e implementação de lógicas podem ser realizadas no nível
de campo, principalmente no caso de equipamentos inteligentes \cite{gaucho}.

As redes de comunicação do nível de controle requerem um grau menor de
determinismo em relação ao nível de campo. Diferem das redes de campo
principalmente em relação à menor freqüência de atualização de dados e no
aumento do tamanho de mensagens. Os protocolos usados nesse nível de produção,
são geralmente baseados no padrão IEEE 802.3, dotados de mecanismos de
atendimento de certas restrições associadas a sistemas de de tempo real
\cite{gaucho}.

\subsection{Nível de Planta}

O nível de planta responde por tarefas de coleta, processamento, armazenamento e
gerenciamento dos dados utilizados pelas atividades corporativas, ou seja, no
domínio da unidade fabril. Para isto, em geral mantém uma base de dados global
onde são guardadas informações sobre a produção, que são utilizados para fins de
análise, histórico e decisões estratégicas da organização industrial. Além
disso, é neste nível que são executados os planos de produção e realizados os
diagnósticos dos elementos do próprio nível e dos níveis inferiores da estrutura
hierárquica \cite{gaucho}.

Neste caso, as mensagens têm larguras que podem chegar à ordem de
\textit{megabytes} e não há necessidade de determinismo na rede, pois as
atividades do nível de planta têm caráter eminentemente gerencial e
administrativo, priorizando o volume de informações ao tempo de resposta
\cite{gaucho}.

\section{Modelo de Referência \textit{OSI}}

A \textit{ISO} (\textit{International Standards Organization}) aprovou, no
início dos anos 80, um modelo que viria facilitar a comunicação entre
dispositivos heterogêneos: o modelo de referência \textit{OSI} (\textit{Open
Systems Interconnection}).

No modelo \textit{OSI} são definidos diferentes estágios que um dado deve passar
para trafegar de um dispositivo a outro em uma rede. Por ser um modelo aberto,
ou seja, por ter suas interfaces definidas publicamente, permitiu que
fabricantes produzissem seus dispositivos de acordo com o padrão definido. Até
então, cada fabricante definia um protocolo de comunicação próprio para seus
dispositivos e, sendo um protocolo fechado, não era possível a interconexão com
dispositivos de outros fabricantes \cite{livro:tanem}.

No modelo \textit{OSI} são definida camadas que possuem funções específicas,
cada uma tem a propriedade de utilizar as funções da camada inferior e fornecer
funcionalidades para a camada logo acima dela. Para um dado chegar de um
\textit{host} a outro em uma rede é necessário que este passe por todas as
camadas definidas pelo modelo. No entanto, é possível pensar que cada camada de
um \textit{host} se comunica diretamente com a mesma camada de um outro
\textit{host}, através de um protocolo definido nesta camada, como demonstrado
na Figura \ref{fig:OSI}. Este esquema de protocolos é conhecido como ``Pilha de
Protocolos''. Uma pilha de protocolos pode ser implementada tanto em hardware
quanto em software, mas normalmente apenas as camadas mais inferiores são
implementadas em hardware \cite{livro:tanem}.

\figura{Modelo de Referência \textit{OSI} \cite{livro:tanem}}{OSI}
{images/modeloosi}{0.75}

As funções de cada camada são apresentadas a seguir.

\subsection{Camada Física}

O nível físico tem a função de transmitir uma seqüência de \textit{bits} através
de um canal de comunicação \cite{livro:tanem}. As funções típicas dos protocolos
deste nível são para fazer com que um \textit{bit} ``1'' transmitido por uma
estação seja entendido pelo receptor como \textit{bit} ``1'' e não como
\textit{bit} ``0''. Assim, este nível trabalha basicamente com as
características mecânicas e elétricas do meio físico, como por exemplo:

\begin{itemize}

\item Número de \textit{volts} que devem representar os níveis lógicos ``1'' e
``0'';

\item Velocidade máxima da transmissão;

\item Transmissão \textit{simplex}, \textit{half-duplex}, ou
\textit{full-duplex};

\item Número de pinos do conector e utilidade de cada um;

\item Diâmetro dos condutores.

\end{itemize}

Os protocolos deste nível são os que realizam a codificação/decodificação de
símbolos e caracteres em sinais elétricos lançados no meio físico, que fica logo
abaixo dessa camada. Como exemplo destes protocolos temos o \textit{RS-232C},
\textit{X.21} (para redes com transmissão digital), \textit{X.21bis} (para redes
com transmissão analógica), codificação Manchester, \textit{SONET}
(\textit{Synchronous Optical Network}), \textit{SDH} (\textit{Synchronous
Digital Hierarchy}) entre outros \cite{livro:tanem}.

\subsection{Camada de Enlace}

A Camada de Enlace fornece os meios para transferência de dados entre dois
dispositivos da rede e de detectar e possivelmente corrigir qualquer erro que
possa vir a ocorrer na Camada Física \cite{livro:tanem}. O protocolo mais
conhecido desta camada atualmente é o \textit{Ethernet} \cite{livro:tanem}. Em
alguns protocolos, esta camada pode ser divida em uma sub-camada de Controle de
Acesso ao Meio (\textit{Medium Access Control -- MAC}) e uma sub-camada de
Controle Lógico do Link (\textit{Logical Link Control -- LLC}).

\subsection{Camada de Rede}

A Camada de Rede possui a função de controlar a operação da rede de um modo
geral. O principal aspecto é executar o roteamento dos pacotes entre fonte e
destino, principalmente quando existem caminhos diferentes para conectar entre
dois nós da rede \cite{livro:tanem}. Em redes de longa distância é comum que a
mensagem chegue do nó fonte ao nó destino passando por diversos nós
intermediários no meio do caminho, e é tarefa da Camada de Rede escolher o
melhor caminho para essa mensagem.

Se muitos pacotes estão sendo transmitidos através dos mesmos caminhos, eles vão
diminuir o desempenho global da rede, formando gargalos. O controle de tais
congestionamentos também é tarefa da Camada de Rede.

Exemplos de protocolos dessa camada são o \textit{IPX} (\textit{Internetwork
Packet Exchange}), o \textit{IP}  (\textit{Internet Protocol}), que pertence à
família de protocolos \textit{TCP/IP} e o \textit{PLP} (\textit{Packet Layer
Protocol}), referenciado no modelo OSI e utilizado nas redes \textit{X.25}
\cite{livro:tanem}.

\subsection{Camada de Transporte}

A Camada de Transporte inclui funções relacionadas com conexões entre a máquina
fonte e máquina destino, segmentando os dados em unidades de tamanho apropriado
para utilização pela Camada de Rede \cite{livro:tanem}.

Sob condições normais, a Camada de Transporte cria uma conexão distinta para
cada conexão de transporte requisitada pelo nível superior. Se a conexão de
transporte requisitada necessita uma alta taxa de transmissão de dados, este
nível pode criar múltiplas conexões de rede, dividindo os dados através da rede
para aumentar a velocidade de transmissão. Por outro lado, se é caro manter uma
conexão de rede, a camada de transporte pode multiplexar várias conexões de
transporte na mesma conexão de rede, a fim de reduzir custos. Em ambos os casos,
a camada de transporte deixa essa multiplexação transparente ao nível superior.

Como exemplos de protocolos desta camada, temos o \textit{TP4}
\cite{livro:tanem}, que foi definido pela \textit{ISO}. Além desse, temos os
protocolos TCP (\textit{Transmission Control Protocol}) e UDP (\textit{User
Datagram Protocol}), que são especificados pelo modelo \textit{TCP/IP}
\cite{livro:tanem}.

\subsection{Camada de Sessão}

A Camada de Sessão tem a função administrar e sincronizar diálogos (sessões)
entre dois dispositivos \cite{livro:tanem}. Este nível oferece dois tipos
principais de diálogo: \textit{half-duplex} (pode receber e enviar dados, mas
não simultâneamente) e \textit{full-duplex} (pode receber e enviar dados
simultaneamente).

Uma sessão permite transporte de dados de uma maneira mais refinada que a Camada
de Transporte oferece. Uma sessão pode ser aberta entre duas estações a fim de
permitir a um usuário se logar em um sistema remoto ou transferir um arquivo
entre essas estações. Os protocolos deste nível tratam de sincronizações em
diálogos, possibilitando interrupções e retornos. Caso ocorram erros, o diálogo
deve ser retomado a apartir do ponto de sicronização \cite{livro:tanem}.

\subsection{Camada de Aplicação}

A função da Camada de Aplicação é assegurar que a informação seja transmitida de
tal forma que possa ser entendida e usada pelo receptor \cite{livro:tanem}.
Dessa forma, este nível pode modificar a sintaxe da mensagem, preservando a sua
semântica. Por exemplo, uma aplicação pode gerar uma mensagem em \textit{ASCII}
mesmo que a estação interlocutora utilize outra forma de codificação (como
\textit{EBCDIC}). A tradução entre os dois formatos é feita nesta camada. O
nível de apresentação também é responsável por outros aspectos da representação
dos dados, como criptografia e compressão de dados \cite{livro:tanem}.
