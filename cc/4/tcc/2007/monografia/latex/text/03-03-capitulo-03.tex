\chapter{\textit{Modbus}}
\label{cap:modbus}

\section{Introdução}

Em meados dos anos 60 o sinal analógico de 4-20 mA (miliampére) foi desenvolvido
e utilizado para realizar o controle de dispositivos industriais.
Aproximadamente em 1980, os sensores inteligentes começaram a ser desenvolvidos
e implementados aplicando um controle digital. Isso motivou a necessidade de
integrar vários tipos de instrumentações digitais em campos de comunicações,
visando otimizar a performance dos sistemas \cite{fieldbus}. Dessa forma se
tornou óbvio que um padrão era necessário para formalizar o controle dos
dispositivos inteligentes. \textit{Fieldbus} é um termo genérico empregado para
descrever tecnologias de comunicação industrial; o termo \textit{fieldbus}
abrange muitos diferentes protocolos para redes industriais. Tal tecnologia é
usada na indústria para substituir o sinal analógico de 4-20 mA (miliampére)
\cite{fieldbus}.

A iniciativa para criação do \textit{fieldbus} partiu das seguintes
organizações: \textit{Instrument Society of America} (ISA),
\textit{International Electrotechnical Commission} (IEC), \textit{Profibus}
(\textit{German National Standard}) e FIP (\textit{French National Standard}),
formando a IEC/ISA SP50 \textit{Fieldbus Committee} \cite{fieldbus}.

O padrão a ser desenvolvido deveria integrar uma grande variedade de
dispositivos de controle. A tecnologia teve como principais objetivos melhorar a
qualidade e reduzir custos. Com a tecnologia \textit{fieldbus} há uma economia
significativa na fiação empregada, dado que usando o sinal analógico de 4-20mA é
necessária que cada dispositivo tenha seu próprio conjunto de fios e seu próprio
ponto de conexão. \textit{Fieldbus} elimina tal necessidade empregando um
esquema que necessita somente de um par trançado \cite{fieldbus}.

\section{Histórico}

Modbus é um protocolo da camada de aplicação, posicionado no nível 7 do modelo
OSI, que prove comunicação cliente/servidor entre dispositivos conectados em
diferentes tipos de barramentos ou redes \cite{man:modbus1}.

Modbus foi desenvolvido por uma empresa chamada Modicon (atualmente Schneider
Electric) em 1979. Sua especificação foi mantida aberta desde o início. Além da
iniciativa de mater a especificação aberta a Schneider ajudou no desenvolvimento
de uma organização de usuários e desenvolvedores independentes chamada
Modbus-IDA.

Modbus-IDA é uma organização sem fins lucrativos que reúne usuários e
fornecedores de dispositivos de automação que visam a adoção do pacote de
protocolos Modbus e a evolução da arquitetura de endereçamento para sistemas de
automação distribuídos em vários segmentos de mercado. A Modbus-IDA fornece a
infra-estrutura para obter e compartilhar informações sobre os protocolos, suas
aplicações e a certificação de dispositivos visando simplificar a implementação
pelos usuários.

\section{Modelos}

Existem atualmente três modelos de implementação do protocolo Modbus
\cite{man:modbus1}:

\begin{itemize}

\item \textbf{Modbus TCP/IP}: É utilizado para comunicação entre sistemas de
supervisão e controladores lógicos programáveis. O protocolo Modbus é
encapsulado no protocolo TCP/IP e transmitido através de redes padrão ethernet.

\item \textbf{Modbus Plus}: É usado para comunicação entre controladores lógicos
programáveis, módulos de entrada e saída, chaves de partida eletrônica de
motores, e interfaces homem máquina. Normalmente o meio físico suporta grandes
taxas transmissão.

\item \textbf{Modbus Padrão}: É usado para comunicação dos controladores lógicos
programáveis com os dispositivos de entrada e saída de dados, instrumentos
eletrônicos inteligentes (IEDs), como relés de proteção, controladores de
processo, e atuadores de válvulas. o meio físico mais utilizado é o EIA232 ou
EIA485 em conjunto com o protocolo mestre-escravo.

\end{itemize}

Na Figura \ref{fig:mb1} é ilustrada a pilha de comunicação modbus.

\figura{Pilha de comunicação Modbus \cite{man:modbus1}}{mb1}{images/mb1}{0.5}

A Figura \ref{fig:mb2} ilustra um exemplo de arquitetura de rede e a forma como
diversos tipos de dispositivos podem utilizar Modbus para iniciar uma operação
remota. Uma comunicação pode ser realizada igualmente através de comunicação
serial como em uma rede Ethernet TCP/IP. Um \textit{gateway} permite a
comunicação entre vários tipos de barramentos ou redes.

\figura{Arquitetura de rede Modbus \cite{man:modbus1}}{mb2}{images/mb2}{0.5}

\section{Descrição do Protocolo}

O protocolo Modbus define o \textit{Protocol Data Unit} (PDU) independente das
camadas de comunicação de outro nível. Informações adicionais podem ser
acrescentadas na \textit{Application Data Unit} (ADU), se forem necessárias por
barramentos específicos ou rede. É denominado \textit{frame} a união da PDU e da
ADU, que são descritas na Figura \ref{fig:mb3}.

\figura{\textit{Frame} Modbus \cite{man:modbus1}}{mb3}{images/mb3}{0.5}

A ADU Modbus é construída pelo cliente (mestre) que inicia a transação Modbus. O
campo \textit{Function code} indica ao servidor {escravo} qual ação executar. O
\textit{Function code} é representado por um \textit{byte}, que pode variar de 1
à 255, o que vai de 128 até 255 são reservados para resposta de exceção.
\textit{Sub-function codes} são adicionados ao campo \textit{data} para definir
sub-funções contidas em uma função \cite{man:modbus2}.

O campo \textit{data} enviado do dispositivo cliente para o dispositivo servidor
contém informações adicionais que o servidor utiliza para executar a ação
definida pelo \textit{function code}. Pode incluir endereço de registros,
quantidade de itens serem manipulados, ou contador da quantidade atual de
\textit{bytes} no campo \cite{man:modbus2}.

O campo \textit{data} pode não existir, ou ter comprimento igual a zero, em
certos tipos de requisição, neste caso o servidor não executa nenhuma
solicitação de informação adicional. Apenas o \textit{function code} especifica
a ação \cite{man:modbus2}.

Se não ocorrerem erros relacionados com a função requisitada o campo
\textit{data} respondido pelo servidor ao cliente conterá o dado requisitado. Se
ocorrerem erros o campo conterá o código de exceção que o servidor pode usar
para determinar a próxima ação a ser tomada \cite{man:modbus2}.

Quando um servidor responde a um cliente, o campo \textit{function code} é
utilizado para indicar o erro ocorrido, se não houverem erros o servidor apenas
repete o \textit{function code} original \cite{man:modbus2}.

A Figura \ref{fig:mb4} ilustra uma transação livre de erros.

\figura{Transação Modbus livre de erros \cite{man:modbus1}}{mb4}{images/mb4}
{0.5}

A Figura \ref{fig:mb5} ilustra uma transação com erros.
\figura{Transação Modbus com erros \cite{man:modbus1}}{mb5}{images/mb5}{0.5}
