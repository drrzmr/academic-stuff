#!/usr/bin/env python

from cherrypy import request
from cherrypy import HTTPError

class resource(object):

	exposed = True
	content = None
	counter = 0

	def __init__(self,content = None):
		self.content = content

	def getCounter(self):
		self.counter += 1
		return self.counter

root = resource()			
