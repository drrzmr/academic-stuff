#!/usr/bin/env python


class Relogio(object):

	estacao = 0
	array = {}

	def __init__(self,estacao,N):
		self.estacao = estacao
		for i in range(1,N+1):
			self.array[i] = 0

	def ticket(self):
		self.array[self.estacao] += 1

	def get_timestamp(self):
		return tuple(self.array.values())

	def update(self,v2):

		count = 1
		for k,v in self.array.iteritems():
			if count != self.estacao:
				self.array[k] = max(v,v2[count-1])
			count += 1
