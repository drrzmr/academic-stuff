#!/usr/bin/env python

import cherrypy

ip = '0.0.0.0'
numero_de_estacoes = 3
estacao = 1

port_map = {}
port_map[1] = 8081
port_map[2] = 8082
port_map[3] = 8083

conf = {
	'global' : {
		'server.socket_host': ip,
		'server.socket_port': port_map[estacao],
	},
	'/' : {
		'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
		'tools.response_headers.on': True,
		'tools.response_headers.headers': [('Content-Type','application/json')],
	}
}

content_type = (
	'application/json',
	'application/json; charset=UTF-8'
)

def getPortStr(estacao):

	return str(port_map[estacao])
